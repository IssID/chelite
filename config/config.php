<?php
$route = require_once __DIR__ . '/route.php';
$db = require_once __DIR__ . '/db.php';

return [
    'brandName' => 'CheLite Framework',
    'route' => $route,
    'db' => $db,
];