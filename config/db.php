<?php
return [
    'user' => getenv('DB_USER'),
    'pass' => getenv('DB_PWD'),
    'pdo' => getenv('DB_DSN'),
    'char_set' => 'utf8mb4',
];