<?php
return [
    '/' => 'MainController/index/',
    '/main' => 'MainController/main/',
    '/test' => 'MainController/test/',
    '/widget' => 'MainController/widget/',
    '/login' => 'MainController/login/',
    '/logout' => 'MainController/logout/',
    '/registration' => 'MainController/registration/',
    '/link' => 'MainController/link/',

    //api
    'GET /api/' => 'DefaultController/index/',

    // Добавление/Редактирование/Удаление категории;
    'GET /api/category' => 'CategoryController/getCategory/',
    'GET /api/category/:num' => 'CategoryController/getCategory/$1',
    'POST /api/category' => 'CategoryController/setCategory/',
    'PUT /api/category/:num' => 'CategoryController/editCategory/$1',
    'DELETE /api/category/:num' => 'CategoryController/deleteCategory/$1',

    // Добавление/Редактирование/Удаление товара.
    'GET /api/product' => 'ProductController/getProduct/',
    'GET /api/product/:num' => 'ProductController/getProduct/$1',
    'POST /api/product' => 'ProductController/setProduct/',
    'PUT /api/product/:num' => 'ProductController/editProduct/$1',
    'DELETE /api/product/:num' => 'ProductController/deleteProduct/$1',

    // Получение списка всех активных категорий (с подсчетом количества товара в каждой категории);
    'GET /api/activeCategory' => 'ProductCategoryController/getActiveCategory/',
    // Получение списка активных товаров в конкретной категории (с подсчетом количества товара);
    'GET /api/activeProductsByCategory/:num' => 'ProductCategoryController/getActiveProductsByCategoryId/$1',

];
