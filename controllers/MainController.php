<?php

namespace app\controllers;

use app\core\Controllers\Controller;
use app\models\Auth;

class MainController extends Controller
{
    /**
     * @throws \Exception
     */
    public function indexAction()
    {
        $this->layout = 'empty';
        $this->render('main/index');
    }

    public function __construct($action)
    {
        // Проверяем авторизацию
        if (!in_array($action, ['loginAction', 'registrationAction'])) {
            if (!Auth::isAuth()) {
                $this->redirect('/login');
            }
        }
    }

    /**
     * @throws \Exception
     */
    public function mainAction()
    {
        $this->render('main/main', [
            'data' => [
                'key1' => 'value1',
                'key2' => 'value2',
                'key4' => $this,
                'query' => ($get = $this->getGet()) ? $get : null,
            ],
            'method' => __METHOD__,
        ]);
    }

    /**
     * @throws \Exception
     */
    public function testAction()
    {
        $this->render('main/main', [
            'data' => [
                'key1' => 'value1',
                'User' => Auth::getAuthUser(),
                'key4' => $this,
                'query' => ($get = $this->getGet()) ? $get : null,
            ],
            'method' => __METHOD__,
        ]);
    }

    /**
     * @param $query
     * @throws \Exception
     */
    public function loginAction($query = [])
    {
        if ($post = $this->getPost()) {
            if (isset($post['email']) && !empty($post['email'])) {
                if (Auth::login($post)) {
                    $this->redirect('/main');
                }
            }
        }
        $this->layout = 'bootstrap';
        return $this->render('main/login');
    }

    /**
     * Выход
     */
    public function logoutAction()
    {
        Auth::logout();
        $this->redirect('/login');
    }

    /**
     * Регистрация пользователя
     *
     * @param $query
     * @throws \Exception
     */
    public function registrationAction($query = [])
    {
        $this->layout = 'bootstrap';
        if ($post = $this->getPost()) {
            if (Auth::registration($post)) {
                $this->redirect('/login');
            }
        }
        return $this->render('main/registration');
    }

    /**
     * @throws \Exception
     */
    public function widgetAction()
    {
        $this->render('main/widget', [
            'data' => [
                'key1' => 'value1',
                'key2' => 'value2',
                'key4' => $this,
                'query' => ($get = $this->getGet()) ? $get : null,
            ],
            'method' => __METHOD__,
        ]);
    }
}