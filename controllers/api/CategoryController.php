<?php

namespace app\controllers\api;

use app\core\Controllers\ApiController;
use app\models\Category;
use Exception;

/**
 * API Контроллер категорий
 */
class CategoryController extends ApiController
{
    /**
     * Получить все категории или одно по id
     *
     * @param int|null $par
     * @return array
     * @throws Exception
     */
    public function getCategoryAction(int $par = null)
    {
        $model = new Category();
        if (!$par) {
            if (!$result = $model->findAll()) {
                self::showError($model->getErrors());
            }
            return $result;
        }
        if (!$result = $model->findById($par)) {
            self::showError($model->getErrors());
        }
        return $result;
    }

    /**
     * Добавить категорию
     *
     * @return array
     * @throws Exception
     */
    public function setCategoryAction()
    {
        if ($post = $_POST) {
            $model = new Category();
            if ($model->insert($post)) {
                return ['success' => true];
            }
            self::showError($model->getErrors());
        }
        self::showError('Не удалось получить данные', 400);
    }

    /**
     * Редактировать категорию по id
     *
     * @param int $id
     * @return bool[]|false[]
     * @throws Exception
     */
    public function editCategoryAction(int $id)
    {
        $data = $this->getPut();
        if (!empty($data)) {
            $model = new Category();
            if ($model->updateById($id, $data)) {
                return ['success' => true];
            }
            self::showError($model->getErrors());
        }
        self::showError('Не удалось получить данные', 400);
    }

    /**
     * Удалить категорию по id
     *
     * @param int $id
     * @return bool[]|false[]
     * @throws Exception
     */
    public function deleteCategoryAction(int $id)
    {
        $model = new Category();
        if ($model->deleteById($id)) {
            return ['success' => true];
        }
        self::showError($model->getErrors());
    }
}