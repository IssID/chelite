<?php

namespace app\controllers\api;

use app\core\Controllers\ApiController;
use app\models\Category;
use Exception;

/**
 * API Контроллер
 */
class DefaultController extends ApiController
{
    /**
     * @return string[]
     */
    public function indexAction()
    {
        return ['API'];
    }
}