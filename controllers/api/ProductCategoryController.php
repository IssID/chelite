<?php

namespace app\controllers\api;

use app\core\Controllers\ApiController;
use app\models\ProductCategory;
use Exception;

class ProductCategoryController extends ApiController
{
    /**
     * Получение списка всех активных категорий (с подсчетом количества товара в каждой категории);
     *
     * @return array
     * @throws Exception
     */
    public function getActiveCategoryAction()
    {
        $model = new ProductCategory();
        if (!$result = $model->getActiveCategoryWithProductCount()) {
            self::showError($model->getErrors());
        }
        return $result;
    }

    /**
     * Получение списка активных товаров в конкретной категории (с подсчетом количества товара);
     *
     * @param int $id
     * @return array
     * @throws Exception
     */
    public function getActiveProductsByCategoryIdAction(int $id)
    {
        $model = new ProductCategory();
        if (!$result = $model->getActiveProductsByCategoryId(['id' => $id])) {
            self::showError($model->getErrors());
        }
        return $result;
    }

}