<?php

namespace app\controllers\api;

use app\core\Controllers\ApiController;
use app\models\Product;
use Exception;

class ProductController extends ApiController
{
    /**
     * Получить все категории или одно по id
     *
     * @param int|null $par
     * @return array
     * @throws Exception
     */
    public function getProductAction(int $par = null)
    {
        $model = new Product();
        if (!$par) {
            if (!$result = $model->findAll()) {
                self::showError($model->getErrors());
            }
            return $result;
        }
        if (!$result = $model->findById($par)) {
            self::showError($model->getErrors());
        }
        return $result;
    }

    /**
     * Добавить запись
     *
     * @return array
     * @throws Exception
     */
    public function setProductAction()
    {
        if ($post = $_POST) {
            $model = new Product();
            if ($model->insert($post)) {
                return ['success' => true];
            }
            self::showError($model->getErrors());
        }
        self::showError('Не удалось получить данные', 400);
    }

    /**
     * Редактировать продукт по id
     *
     * @param int $id
     * @return array
     * @throws Exception
     */
    public function editProductAction(int $id)
    {
        $data = $this->getPut();
        if (!empty($data)) {
            $model = new Product();
            if ($model->updateById($id, $data)) {
                return ['success' => true];
            }
            self::showError($model->getErrors());
        }
        self::showError('Не удалось получить данные', 400);
    }

    /**
     * Удалить категорию по id
     *
     * @param int $id
     * @return array
     * @throws Exception
     */
    public function deleteProductAction(int $id)
    {
        $model = new Product();
        if ($model->deleteById($id)) {
            return ['success' => true];
        }
        self::showError($model->getErrors());
    }

}