<?php

namespace app\core;

class App
{
    public static $config;

    public function __construct($config = [])
    {
        self::$config = self::$config ?? $config;
    }

    public function run()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if (!isset(self::$config['route'])) {
            throw new \Exception('Не удалось получить route', 404);
        }
        $router = new Router();
        $router->addRoute(self::$config['route']);
        $router->dispatch();
    }
}