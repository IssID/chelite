<?php

namespace app\core;

use Exception;
use PDO;
use PDOException;

/**
 * Подключение к базе данных
 */
class DataBase
{
    /** @var $db PDO|null */
    private static $db = null;

    /** заглушка */
    protected function __construct(){}
    /** заглушка */
    protected function __clone(){}
    /** заглушка */
    public function __wakeup(){}

    /**
     * Получить подключение к базе данных
     *
     * @return PDO
     * @throws Exception
     */
    public static function getDb(): PDO
    {
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $confDB = require __DIR__ . '/../config/db.php';
        try {
            static::$db = static::$db ?? (static::$db = new PDO($confDB['pdo'], $confDB['user'], $confDB['pass'], $opt));
            static::$db->exec("SET CHARSET " . $confDB['char_set']);
            return static::$db;
        } catch (PDOException $e) {
            Errors::showError($e->getMessage(), $e->getCode());
            die();
        }
    }
}