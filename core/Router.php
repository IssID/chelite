<?php

namespace app\core;

use Exception;

/**
 * Роутер, обрабатывает http запросы и вызывает нужный action в контроллере
 */
class Router
{
    /** @var array Список роутов */
    private $routes = [];
    /** @var array Параметры */
    private $params = [];
    /** @var bool Является ли апи */
    private $isApi = false;
    /** @var string регулярное для парсинга ключей query запроса */
    private $queryPattern = '|^([a-zA-Z0-9]+)\[([a-zA-Z0-9_-]+)\]$|';

    /**
     * Добавить маршрут в список
     *
     * @param $route
     * @param null $destination
     */
    public function addRoute($route, $destination = null)
    {
        if ($destination != null && !is_array($route)) {
            $route = [$route => $destination];
        }

        $this->routes = array_merge($this->routes, $route);
    }

    /**
     * Обработка URL
     *
     * @throws Exception
     */
    public function dispatch()
    {
        $arrayUri = explode('?', $_SERVER["REQUEST_URI"]);
        $uri = reset($arrayUri);
        $requestedUrl = $this->getRequestedUrl($uri);

        $this->checkApi($requestedUrl);
        $requestedUrl = $_SERVER['REQUEST_METHOD'] . " " . $requestedUrl;

        // если URL и маршрут полностью совпадают
        if (isset($this->routes[$requestedUrl])) {
            $this->params = $this->splitUrl($this->routes[$requestedUrl]);
            $this->executeAction();
        }

        foreach ($this->routes as $route => $uri) {
            // Заменяем wildcards на рег. выражения
            if (strpos($route, ':') !== false) {
                $route = str_replace(':any', '(.+)', str_replace(':num', '([0-9]+)', $route));
            }

            if (preg_match('#^' . $route . '$#', $requestedUrl)) {
                if (strpos($uri, '$') !== false && strpos($route, '(') !== false) {
                    $uri = preg_replace('#^' . $route . '$#', $uri, $requestedUrl);
                }
                $this->params = $this->splitUrl($uri);
                break;
            }
        }

        // Если не удалось найти роут с типом запроса, ищем без типа
        if (empty($this->params)) {
            $requestedUrl = str_replace($_SERVER['REQUEST_METHOD'] . " ", '', $requestedUrl);
            if (isset($this->routes[$requestedUrl])) {
                $this->params = $this->splitUrl($this->routes[$requestedUrl]);
            }
        }

        $this->setQueryParams($arrayUri);
        $this->executeAction();
    }

    /**
     * Разделить переданный URL на компоненты
     *
     * @param string $url
     * @return array|false|string[]
     */
    private function splitUrl(string $url)
    {
        return preg_split('/\//', $url, -1, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * Устанавливаем параметры из query запроса
     *
     * @param $arrayUri
     */
    private function setQueryParams($arrayUri)
    {
        if (count($arrayUri) > 1) {
            $query = end($arrayUri);
            $queryParams = $this->getQueryParams($query);
            $this->params = array_merge_recursive($this->params, $queryParams);
        }
    }

    /**
     * Получить параметры из query
     *
     * @param $query
     * @return array
     */
    private function getQueryParams($query)
    {
        $result = [];
        $items = explode('&', $query);
        foreach ($items as $item) {
            if (!empty($item)) {
                list($key, $value) = explode('=', $item);
                $value = urldecode($value);
                $key = urldecode($key);
                if (preg_match($this->queryPattern, $key, $res)) {
                    $result[$res[1]][$res[2]] = $value;
                } else {
                    $result[$key] = $value;
                }
            }
        }
        return $result;
    }

    /**
     * @param $uri
     * @return string
     */
    private function getRequestedUrl($uri)
    {
        $requestedUrl = urldecode(rtrim($uri, '/'));
        if (empty($requestedUrl)) {
            $requestedUrl = $uri;
        }
        return $requestedUrl;
    }

    /**
     * @param $requestedUrl
     */
    private function checkApi($requestedUrl)
    {
        $arr = explode('/', $requestedUrl);
        if (isset($arr[1]) && strtoupper($arr[1]) == 'API') {
            $this->isApi = true;
        }
    }

    /**
     * Запуск соответствующего действия/экшена/метода контроллера
     *
     * @throws Exception
     */
    private function executeAction()
    {
        $controller = isset($this->params[0]) ? $this->params[0] : 'DefaultController';
        if ($this->isApi) {
            $controller = 'app\controllers\api\\' . $controller;
        } else {
            $controller = 'app\controllers\\' . $controller;
        }

        $action = isset($this->params[1]) ? $this->params[1] : 'index';
        $action = $action . 'Action';

        if (!class_exists($controller) || !method_exists($controller, $action)) {
            if ($this->isApi) {
                Errors::showApiError('Not Found The requested URL was not found on this server.', 404);
            } else {
                Errors::notFound();
            }
        }

        $params = array_slice($this->params, 2);

        try {
            $data = call_user_func_array([new $controller($action), $action], $params);
        } catch (\Throwable $err) {
            if (getenv('DEBUG')) {
                Errors::debugError($err);
                exit();
            }
            Errors::Error();
            exit();
        }

        // Выводим апи без рендеринга
        if ($this->isApi) {
            header('Content-type: application/json');
            echo json_encode($data, JSON_UNESCAPED_UNICODE);
            exit();
        }
    }
}
