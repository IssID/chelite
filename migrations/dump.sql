--
-- Описание для таблицы shop_category
--
DROP TABLE IF EXISTS shop_category;
CREATE TABLE shop_category (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  parent INT(11) UNSIGNED NOT NULL COMMENT 'Родительская категория',
  is_enabled TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Флаг активности категории',
  name VARCHAR(255) DEFAULT NULL COMMENT 'Название категории',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 3
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Категории товаров';

--
-- Описание для таблицы shop_product
--
DROP TABLE IF EXISTS shop_product;
CREATE TABLE shop_product (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  is_enabled TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Флаг активности',
  name VARCHAR(255) DEFAULT NULL COMMENT 'Название товара',
  announce VARCHAR(255) DEFAULT NULL COMMENT 'Анонс',
  description TEXT DEFAULT NULL COMMENT 'Описание',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Товары';

--
-- Описание для таблицы shop_product_category
--
DROP TABLE IF EXISTS shop_product_category;
CREATE TABLE shop_product_category (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  category_id INT(11) UNSIGNED NOT NULL COMMENT 'ID категории',
  product_id INT(11) UNSIGNED NOT NULL COMMENT 'ID товара',
  PRIMARY KEY (id),
  INDEX shop_product_category_idx (category_id),
  INDEX shop_product_product_idx (product_id),
  CONSTRAINT FK_shop_product_category_to_category FOREIGN KEY (category_id)
    REFERENCES shop_category(id) ON DELETE NO ACTION ON UPDATE RESTRICT,
  CONSTRAINT FK_shop_product_category_to_product FOREIGN KEY (product_id)
    REFERENCES shop_product(id) ON DELETE NO ACTION ON UPDATE RESTRICT
)
ENGINE = INNODB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица соответствия категорий и товаров';

-- 
-- Вывод данных для таблицы shop_category
--
INSERT INTO shop_category VALUES
(1, 0, 1, 'root'),
(2, 1, 1, 'Бытовая техника');

-- 
-- Вывод данных для таблицы shop_product
--
INSERT INTO shop_product VALUES
(1, 1, 'Холодильник', NULL, NULL);

-- 
-- Вывод данных для таблицы shop_product_category
--
INSERT INTO shop_product_category VALUES
(1, 2, 1);

DROP TABLE IF EXISTS users;
CREATE TABLE users (
   id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
   login VARCHAR(255) NOT NULL UNIQUE COMMENT 'Логин',
   password VARCHAR(255) NOT NULL COMMENT 'Пароль',
   email VARCHAR(255) NOT NULL UNIQUE COMMENT 'Почта',
   access_lvl INT(11) UNSIGNED NOT NULL COMMENT 'Уровень доступа',
   api_token VARCHAR(255) DEFAULT NULL COMMENT 'API token',
   access_token VARCHAR(255) DEFAULT NULL COMMENT 'access token',
   PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
