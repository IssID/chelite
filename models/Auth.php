<?php

namespace app\models;

/**
 * Простая реализация авторизации
 */
class Auth
{
    /** @var Users Авторизованый пользователь */
    private static $user;

    /** заглушка */
    protected function __construct(){}
    /** заглушка */
    protected function __clone(){}
    /** заглушка */
    public function __wakeup(){}

    /**
     * Получить пользователя (авторизованый или пустой)
     *
     * @return \app\core\Models\ActiveRecords|Users|bool|false
     * @throws \Exception
     */
    public static function getAuthUser()
    {
        if (!isset(self::$user)) {
            self::$user = new Users();
        }

        if (isset($_COOKIE['access_token']) && !empty($_COOKIE['access_token'])) {
            self::$user = self::$user->findOne(['access_token' => $_COOKIE['access_token']]);
        } elseif (isset($_SESSION['access_token']) && !empty($_SESSION['access_token'])) {
            self::$user = self::$user->findOne(['access_token' => $_SESSION['access_token']]);
        }

        return self::$user;
    }

    /**
     * Авторизован ли пользователь
     *
     * @return bool
     * @throws \Exception
     */
    public static function isAuth()
    {
        $user = self::getAuthUser();
        if (isset($user->password) && !empty($user->password)) {
            return true;
        }
        return false;
    }

    /**
     * Получить рандомный хеш
     *
     * @param int $length
     * @return string
     * @throws \Exception
     */
    public static function getRandHex($length = 20)
    {
        return bin2hex(random_bytes($length));
    }

    /**
     * Вход
     *
     * @param array $post
     * @return bool
     * @throws \Exception
     */
    public static function login(array $post)
    {
        if (isset($_COOKIE['access_token']) && !empty($_COOKIE['access_token'])) {
            return true;
        } elseif (isset($_SESSION['access_token']) && !empty($_SESSION['access_token'])) {
            return true;
        }

        if (isset($post['email']) && !empty($post['email'])) {
            if (!$user = (new Users())->findOne(['email' => $post['email']])) {
                return false;
            }
        }

        if (!isset($post['password']) || empty($post['password'])) {
            return false;
        }

        if (!password_verify($post['password'], $user->password)) {
            return false;
        }

        $user->access_token = Auth::getRandHex(32);

        if ($user->load((array)$user) && $user->save()) {
            if (isset($post['remember_me']) && !empty($post['remember_me'])) {
                setcookie('access_token', $user->access_token);
            } else {
                $_SESSION['access_token'] = $user->access_token;
            }
            return true;
        }

        return false;
    }

    /**
     * Выход
     */
    public static function logout()
    {
        if (session_status() == PHP_SESSION_ACTIVE) {
            session_destroy();
        }
        unset($_COOKIE['access_token']);
        setcookie('access_token', null, -1, '/');
    }

    /**
     * Регистрация пользователя
     *
     * @param array $post
     * @return bool
     * @throws \Exception
     */
    public static function registration(array $post)
    {
        if (!isset($post['login']) || empty($post['login'])) {
            return false;
        }
        if (!isset($post['email']) || empty($post['email'])) {
            return false;
        }
        if (!isset($post['password']) || empty($post['password'])) {
            return false;
        }

        $user = new Users();
        if (!$user->load($post)) {
            return false;
        }

        $user->password = password_hash($user->password, PASSWORD_DEFAULT);
        $user->access_token = Auth::getRandHex(32);
        $user->api_token = Auth::getRandHex(16);
        $user->access_lvl = Users::USER_LVL;
        if ($user->load((array)$user) && $user->save()) {
            self::$user = $user;
            return true;
        }
        return false;
    }

    /**
     * Получить уровень доступа
     *
     * @return int
     * @throws \Exception
     */
    public static function getAccessLvl()
    {
        $user = self::getAuthUser();
        if (isset($user->access_lvl) && !empty($user->access_lvl)) {
            return $user->access_lvl;
        }
        return Users::GUEST_LVL;
    }

}