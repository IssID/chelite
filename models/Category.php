<?php

namespace app\models;

use app\core\Models\ActiveRecords;

/**
 * Категории
 */
class Category extends ActiveRecords
{
    /**
     * @return string
     */
    static function tableName(): string
    {
        return 'shop_category';
    }

    /**
     * Найти запись по id
     * @param int $id
     * @return mixed
     * @throws \Exception
     */
    public function findById(int $id)
    {
        return $this->findOne(['id' => $id]);
    }

    /**
     * Удалить запись по id
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function deleteById(int $id)
    {
        $query = $this->prepareDelete(self::tableName());
        return $this->execute($query, ['id' => $id]);
    }

    /**
     * Записать данные в базу
     *
     * @param array $params
     * @return bool[]|false
     * @throws \Exception
     */
    public function insert(array $params)
    {
        $query = $this->prepareInsert(self::tableName(), $params);
        return $this->execute($query, $params);
    }

    /**
     * @param int $id
     * @param array $params
     * @return bool
     * @throws \Exception
     */
    public function updateById(int $id, array $params)
    {
        $query = $this->prepareUpdate(self::tableName(), $params);
        $params['id'] = $id;
        return $this->execute($query, $params);
    }
}
