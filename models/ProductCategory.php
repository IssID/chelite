<?php

namespace app\models;

use app\core\Models\ActiveRecords;
use app\core\DataBase;
use Exception;

/**
 * Связь продукта с категорией
 */
class ProductCategory extends ActiveRecords
{
    /**
     * @return string
     */
    static function tableName(): string
    {
        return 'shop_product_category';
    }

    /**
     * Получение списка всех активных категорий (с подсчетом количества товара в каждой категории);
     *
     * @return array|false
     * @throws Exception
     */
    public function getActiveCategoryWithProductCount()
    {
        $query = 'SELECT   sc.id,
                           sc.parent,
                           sc.is_enabled,
                           sc.name,
                           count(spc.category_id) AS products_count
                    FROM shop_category sc
                             LEFT JOIN shop_product_category spc ON sc.id = spc.category_id
                    WHERE is_enabled = true
                    GROUP BY sc.id';
        self::$stmt = DataBase::getDb()->prepare($query);
        if (!self::$stmt->execute()) {
            self::$errors[] = self::$stmt->errorInfo();
            return false;
        }
        return self::$stmt->fetchAll();
    }

    /**
     * Получение списка активных товаров в конкретной категории (с подсчетом количества товара);
     *
     * @param array $params
     * @return array|false
     * @throws Exception
     */
    public function getActiveProductsByCategoryId(array $params)
    {
        $query = 'SELECT   sp.id,
                           sp.is_enabled,
                           sp.name,
                           sp.announce,
                           sp.description
                    FROM shop_product sp
                             LEFT JOIN shop_product_category spc ON sp.id = spc.product_id
                    WHERE sp.is_enabled = true
                      AND spc.category_id=:id';
        self::$stmt = DataBase::getDb()->prepare($query);
        if (!self::$stmt->execute($params)) {
            self::$errors[] = self::$stmt->errorInfo();
            return false;
        }
        return [
            'products_count' => (string)self::$stmt->rowCount(),
            'products' => self::$stmt->fetchAll()
        ];
    }
}
