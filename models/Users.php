<?php

namespace app\models;

use app\core\Models\ActiveRecords;

class Users extends ActiveRecords
{
    // уровни доступа
    const ADMIN_LVL = 99;
    const USER_LVL = 1;
    const GUEST_LVL = 0;

    static function tableName(): string
    {
        return 'users';
    }
}