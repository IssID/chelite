<?php

namespace app\tests\acceptance;

use \AcceptanceTester;

class FirstCest
{
    /**
     * Проверяем на главной странице текст API
     * @param AcceptanceTester $I
     */
    public function frontpageWorks(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->see('IssID');
    }
}
