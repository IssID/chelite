<?php

namespace app\tests\api;

use Codeception\Util\HttpCode;
use ApiTester;

class ApiCest
{
    public function getCategoryAPI(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
        $I->sendGet('/api/category');

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"id":"1","parent":"0","is_enabled":"1","name":"root"}');

    }
}