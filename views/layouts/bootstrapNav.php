<?php
/**
 * @var $content
 * @var $title
 */

use app\models\Auth;
use app\models\Users;

$user = Auth::getAuthUser();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
          integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css"
          integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"
            integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd"
            crossorigin="anonymous"></script>

    <title><?php echo $this->title; ?></title>

</head>
<body>
<div class="container theme-showcase" role="main">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/main"><?php echo $this->brandName; ?></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <?php
                    if (Auth::getAccessLvl() == Users::ADMIN_LVL) {
                        echo '<li><a href="/widget">Widget <span class="sr-only">(current)</span></a></li>';
                        echo '<li><a href="/test">test</a></li>';
                        echo '<li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                                   aria-expanded="false">Dropdown <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/action">Action</a></li>
                                    <li><a href="/action2">Another action</a></li>
                                    <li><a href="/action3">Something else here</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="/action4">Separated link</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="/action5">One more separated link</a></li>
                                </ul>
                            </li>';
                    }
                    ?>
                </ul>
                <?php
                if (Auth::getAccessLvl() == Users::ADMIN_LVL) {
                    echo '<form class="navbar-form navbar-left">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Search">
                            </div>
                            <button type="submit" class="btn btn-default">Submit</button>
                        </form>';
                }
                ?>
                <ul class="nav navbar-nav navbar-right">
                    <?php
                    if (Auth::getAccessLvl() >= Users::USER_LVL) {
                        echo '<li><a href="/about">About</a></li>';
                    }
                    ?>

                    <?php
                    if (!isset($user->login)) {
                        echo '<li><a href="/login">Login</a></li>';
                    } else {
                        echo '<li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                ' . $user->login . '
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/settings">Settings</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="/logout">Logout</a></li>
                            </ul>
                        </li>';
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>
    <br>
    <?php echo $this->content; ?>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        var url = window.location;
        $('ul.nav a[href="' + url + '"]').parent().addClass('active');
        $('ul.nav a').filter(function () {
            return this.href == url;
        }).parent().addClass('active');
    });
</script>
</body>
</html>