<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
      integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css"
      integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"
        integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd"
        crossorigin="anonymous"></script>

<div class="container">

    <?php
    /** @var $error \PhpParser\Error */
    echo '<div class="alert alert-danger alert-dismissible fade in" role="alert">
            <h4>Oh snap! You got an error!</h4>
            <p>' . $error->getMessage() . '</p>
            <p>file: ' . $error->getFile() . ':' . $error->getLine() . '</p>
        </div>';

    foreach ($error->getTrace() as $key => $item) {
        $in = $key < 3 ? "in" : "";
        echo '<div class="panel-group" role="tablist">
                <div class="panel panel-default">
                    <div href="#collapseListGroup' . $key . '" class="panel-heading" role="button" data-toggle="collapse" id="collapseListGroupHeading' . $key . '" >
                        <h4 class="panel-title">' . $item['function'] . '</h4>
                    </div>
                    <div class="panel-collapse collapse ' . $in . '" role="tabpanel" id="collapseListGroup' . $key . '" aria-labelledby="collapseListGroupHeading' . $key . '" aria-expanded="true" style="">
                        <ul class="list-group">';

        if (isset($item['file'])) echo '<li class="list-group-item">file: ' . $item['file'] . '</li>';
        if (isset($item['line'])) echo '<li class="list-group-item">line: ' . $item['line'] . '</li>';
        if (isset($item['function'])) echo '<li class="list-group-item">function: ' . $item['function'] . '</li>';
        if (isset($item['class'])) echo '<li class="list-group-item">class: ' . $item['class'] . '</li>';
        if (isset($item['args']) && !empty($item['args'])) {
            echo '<li class="list-group-item"><pre>args: ';
            print_r($item['args']);
            echo '</pre></li>';
        }
        echo '</ul>
                        <div class="panel-footer"></div>
                    </div>
                </div>
            </div>';
    }
    ?>
</div>
