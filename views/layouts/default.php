<?php
/**
 * @var $content
 * @var $title
 * @var $footer
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
</head>
<body>
<?php echo $content; ?>
<footer>
    <?php echo $footer; ?>
</footer>
</body>
</html>

