<?php
/**
 * @var $content
 * @var $title
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">
    <script src="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.ui.dropdown').dropdown();
        });
    </script>
    <title><?php echo $this->title; ?></title>
</head>
<body>

<div class="ui container">
    <div class="ui small menu">
        <a class="active item">
            Home
        </a>
        <a class="item">
            Messages
        </a>
        <div class="right menu">
            <div class="ui dropdown item">
                Language <i class="dropdown icon"></i>
                <div class="menu">
                    <a class="item">English</a>
                    <a class="item">Russian</a>
                    <a class="item">Spanish</a>
                </div>
            </div>
            <div class="item">
                <div class="ui primary button">Sign Up</div>
            </div>
        </div>
    </div>
    <br>
    <?php echo $this->content; ?>
</div>
</body>
</html>
