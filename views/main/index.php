<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>IssID.pro</title>
    <link rel="stylesheet" href="./assets/onepage/style.css">
</head>
<body>
<div class="parent">
    <div class="block">
        <div class="text-component">
            <div class="text">IssID</div>
        </div>
    </div>
</div>
<script src="./assets/onepage/js/main.js"></script>
</body>
</html>