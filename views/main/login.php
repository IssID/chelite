<style>
    .panel {
        position: fixed;
        width: 400px;
        top: 20%;
        left: calc(50% - 200px);
    }
</style>
<div class="container theme-showcase" role="main">

    <div class="row">
        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <p class="panel-title"><span class="glyphicon glyphicon-lock"></span> Login</p>
                </div>
                <div class="panel-body">
                    <form method="post" action="login">
                        <div class="form-group">
                            <label required for="exampleInputEmail1">Email address</label>
                            <input name="email" type="email" class="form-control" id="exampleInputEmail"
                                   placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input required name="password" type="password" class="form-control" id="exampleInputPassword"
                                   placeholder="Password">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input name="remember_me" type="checkbox">Remember me
                            </label>
                        </div>
                        <button type="submit" class="btn btn-default">Login</button>
                    </form>
                </div>
                <div class="panel-footer">
                    Not Registred? <a href="/registration">Register here</a>
                </div>
            </div>
        </div>
        <div class="col-lg-4"></div>
        <div class="col-lg-4"></div>
    </div>
</div>