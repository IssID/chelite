<?php
$start = microtime(true);
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require __DIR__ . '/../vendor/autoload.php';
(new josegonzalez\Dotenv\Loader(__DIR__ . '/../.env'))->parse()->putenv(true)->define();

$config = require_once __DIR__ . '/../config/config.php';

(new app\core\App($config))->run();

?>
<script>console.log("<?php print_r(microtime(true) - $start .' sec'); ?>");</script>